// Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// Що таке явне та неявне приведення (перетворення) типів даних у JS?

// Цикли потрібні для оптимізяції та упрощення виконання задач

// цикл for використовую коли треба перебирати данні. Цикл while використовую, коли у мене є умова при якій цикл повинен закінчитись

// Неявне перетворення даних відбувається автоматично під час виконання операцій. Наприклад, якщо виконати операцію додавання між числом та рядком, то рядок автоматично буде перетворено в число, якщо це можливо.
// Явне перетворення даних відбувається за допомогою функцій, таких як parseInt(), parseFloat() або String().

//---------------------------------------------------------------------//
let num;
let key = true;
while(key==true){
    if(num == null){
        num = Number(prompt("Введыіть число, яке потрібно перебрати"));
    }else if(isNaN(num)){
        num = Number(prompt("Ви ввели не коректні данні \nВведіть число, яке потрібно перебрати"));
    }else if(num == "" || num == " "){
        num = Number(prompt("Поле не може бути пустим \nВведіть число, яке потрібно перебрати")); 
    }else{
        key = false;
    }
}
let counter = 0;
for(i=1; i<=num; i++){
    if(i%5 == 0){
        console.log(i);
        counter++;
    }
}
if(counter == 0){
    console.log("Sorry, no numbers");
}
//---------------------------------------------------------------------//
let isSimple = true;
let key_2 = true;
let num_2;
while(key_2 == true){
    isSimple = true;
    if(num_2 == null){
        num_2 = Number(prompt("Введіть число"))
    }
    if(isNaN(num_2)){
        num_2 = Number(prompt("Ви ввели не коректні данні \nВведіть число")) 
    }else if(num_2 == "" || num_2 ==" "){
        num_2 = Number(prompt("Поле не може бути пустим \nВведіть число")) 
    }else{
        if(num_2 == 1){
            isSimple = false;
            console.log(`${num_2} - не є простим числом`);
            num_2 = null;
            continue;
        }
        for(i=2; i<num_2; i++){
                if(num_2 % i == 0){
                    isSimple = false;
                    console.log(`${num_2} - не є простим числом`);
                    num_2 = null;
                    break;
                }
        }
        if(isSimple == true){
            console.log(`${num_2} - просте число`);
            key_2 = false;
        } 
    }    
}
//---------------------------------------------------------------------//
let simple_key = true;
let simple_num = true;
let m;
let n;
while(simple_key == true){
    if(m == null){
        m = Number(prompt("Введыніть число m"))
    }else if(isNaN(m)){
        m = Number(prompt("Ви ввели не коректні данні \nВведіть число m"))
    }
    else if(m == "" || m == " "){
        m = Number(prompt("Поле не може бути пустим \nВведіть число m"))
    }

    if(n == null){
        n = Number(prompt("Введыніть число n"))
    }else if(isNaN(n)){
        n = Number(prompt("Ви ввели не коректні данні \nВведіть число n"))
    }else if(n == "" || n == " "){
        n = Number(prompt("Поле не може бути пустим \nВведіть число n"))
    }else if(n <= m){
        n = Number(prompt(`Число n повинно бути більшим за ${m} \nВведіть число n`)) 
    }else{
        simple_key = false;
    }
}
if(m == 1){
    m=2;
}

for(i=m; i<=n; i++){
    simple_num = true;
    for(j=2; j<i; j++){
        if (i % j == 0) {
            simple_num = false;
            break;
        }
    }
    if(simple_num == true){
        console.log(i);
    }
}